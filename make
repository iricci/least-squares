#!/bin/bash
# Setup

# The convenience =make= script allows for batch execution of full workflow and its individual parts. The targets are included using the templating no-web syntax to create different project variants.

export PYTHONWARNINGS="ignore"

. src/logger.sh

line() {
    printf '%.s-' $(seq 1 $(tput cols))
    echo
}

# Exit immediately on error
set -o errtrace
trap "echo; echo ERROR: failed to execute target $1" ERR

# Make sure python environment is activated
# It is not deactivated on exit
[ -d env ] && [ -z $(command -v deactivate) ] && . env/bin/activate

case $1 in
      all)
	  start
	  $0 clean && \
	  $0 setup && \
	  $0 analysis && \
	  $0 plots && \
 	  echo -e "\nWorkflow reproduced!"
	  stop
	  ;;
      setup)
	  echo -e "\nEnvironment setup"; line
	  ./00_setup.sh
	  ;;
      analysis)
	  start
	  echo -e "\nAnalysis"; line
	  for f in 01_* ; do ./$f; done
	  stop
	  ;;
      plots)
	  start
	  echo -e "\nPlots"; line
	  for f in 02_* ; do ./$f; done
	  stop
	  ;;
      clean)
	  echo -e "\nClear the dataset from the artefacts of the analysis"; line
  	  rm -rf data/ plots/
	  ;;
      veryclean)
	  echo -e "\nCompletely clear the dataset and python environment"; line
	  read -p "Are you sure? [y/Y] " -n 1 -r
	  [[ ! $REPLY =~ ^[Yy]$ ]] && exit 0
	  $0 clean
	  rm -rf env/
	  ;;
      *)
	  echo "./make [all|setup|analysis|plots|clean|veryclean]"
	  ;;
esac
