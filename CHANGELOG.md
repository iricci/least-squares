# Changelog
This changelog encompasses all of the developing steps done locally, before using versioning tools. 

## 1.2.0 - 2023/10/11
Minor fixes to `makefile` script and to project structure. This version should
ensure backwards compatibility (hence the minor nature of this update).

### New additions
- Add `plots` and `analysis` steps to `makefile`
### Fixes
- Remove dead steps from `makefile`
- Move `utilities.py` to `src/` and fix all references
- Fix path handling in `src/utilities.py` and its dependencies
### Deprecated
- `timelog` function in `src/utilities.py` (yet to be removed)

## 1.1.0 - 2023/05/04
Adapt existing code to the aforementioned template. No new features were added.

## 1.0.0 - 2023/05/04
Initial commit, based on [this](https://framagit.org/coslo/template-project) template for reproducible research.

## 1.0.0 (LOCAL) - 2022/09/12
First instance of production-like code. Time expense is reasonable, all algorithms work properly, plots are fine. 

### New features
- Add missing plotting routines
### Deprecated
- Remove unused code

## 0.6.0 (LOCAL) - 2022/09/11
### New features
- Add input/output directory mgmt
- Add basic plotting routines

## 0.5.0 (LOCAL) - 2022/09/10
### Fixes
- Fix data logging and display

## 0.4.0 (LOCAL) - 2022/08/29
### Fixes
- Fix linear LS 
- Fix speed issues in numerical LS by vectorizing loops
### New features
- Add hypoteses tests
 
## 0.3.0 (LOCAL) - 2022/08/27
### New features
- Add numerical LS fit using loops 
- Add numerical LS fit error estimation 

## 0.2.0 (LOCAL) - 2022/08/24
### New features
- Add linear LS fit

## 0.1.0 (LOCAL) - 2022/08/23
First code instance, based on existing code for non linear LS fitting.
