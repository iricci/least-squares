#!/usr/bin/env python
# Analysis

# Python script to generate plots of linear and numerical LS fits, including a confidence ellipse for numerical LS. Plots go into =plots=.

import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sst

import src.utilities as ut


def plot_MMQL(data, nstep, m, q,
              xmin=0., xmax=0.026, ymin=0.004, ymax=0.042):
    """
    Plot MMQL.

    Parameters: data:   list - starting data for linear relation y = mx+q (x, y, sy)
    """
    x = np.linspace(0, max(data[0]+0.005), 100)
    y = m*x + q
    fig = plt.figure(figsize=(20, 20))
    plt.grid()
    plt.scatter(data[0], data[1])
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.errorbar([float(data[0][i]) for i in range(len(data[0]))], [float(
        data[1][i]) for i in range(len(data[1]))], yerr=float(data[2][0]), fmt='o')
    plt.plot(x, y)
    plt.title('Fit lineare con parametri stimati con MMQL')
    plt.savefig(os.path.join(os.path.dirname(__file__),
                r'plots/fitlinMMQL_{}.svg'.format(nstep)), bbox_inches='tight')
    plt.close(fig)


def plot_ellipse(nstep, m2_min, q2_min, m_err, q_err):
    """
    Plot numerical MMQ confidence ellipse.

    Parameters: data:   list - starting data for linear relation y = mx+q (x, y, sy)
    """

    fig = plt.figure(figsize=(20, 20))
    plt.grid()
    plt.scatter(m2_min, q2_min)
    plt.scatter(m_err, q_err)
    plt.title('Ellisse $Q^2 = 1$ e $(\hat{m}, \hat{q})$')
    plt.savefig(os.path.join(os.path.dirname(__file__),
                r'plots/ellisseMMQnum_{}.png'.format(nstep)), bbox_inches='tight')
    plt.close(fig)


def plot_MMQN(data, nstep, m2_min, q2_min,
              xmin=0., xmax=0.026, ymin=0.004, ymax=0.042):
    """
    Plot numerical MMQ.

    Parameters: data:   list - starting data for linear relation y = mx+q (x, y, sy)
    """
    x = np.linspace(0, max(data[0]+0.005), 100)
    y = m2_min*x + q2_min
    fig = plt.figure(figsize=(20, 20))
    plt.grid()
    plt.scatter(data[0], data[1])
    plt.xlim(0., 0.026)
    plt.ylim(0.004, 0.042)
    plt.errorbar([float(data[0][i]) for i in range(len(data[0]))], [float(
        data[1][i]) for i in range(len(data[1]))], yerr=float(data[2][0]), fmt='o')
    plt.plot(x, y)
    plt.title('Fit lineare con parametri stimati con MMQ')
    plt.savefig(os.path.join(os.path.dirname(__file__),
                r'plots/fitlinMMQ_{}.svg'.format(nstep)), bbox_inches='tight')
    plt.close(fig)


def plot_TDI(nstep, gdl_MMQ, t_alpha_005, t_chi_MMQ, t_chi_MMQL):
    t = np.linspace(0, 22, 200)
    f_chi2 = sst.chi2.pdf(t, gdl_MMQ)

    fig = plt.figure(figsize=(20, 20))
    plt.grid()
    plt.plot(t, f_chi2)
    plt.vlines(t_alpha_005, ymin=0, ymax=0.14)
    plt.vlines(t_chi_MMQ, ymin=0, ymax=0.14, color='r')
    plt.vlines(t_chi_MMQL, ymin=0, ymax=0.14, color='g')
    plt.title('Test di $\chi^2$ - Livello di significatività 0.05')
    plt.savefig(os.path.join(os.path.dirname(__file__),
                r'plots/tdi_{}.svg'.format(nstep)), bbox_inches='tight')
    plt.close(fig)


# Read back parameters and data from file
input_directory = r'input/dati_molle_1.5_edited.xlsx'
names = ['G', 'M', 'S']
data = ut.excel(input_directory, names)

nstep, \
    m_linear, sm_linear, q_linear, sq_linear, cov_linear, \
    m_numerical, sm_numerical, q_numerical, sq_numerical, \
    t_MMQ, t_MMQL, gdl_MMQ, t_alpha, tm_corr, tq_corr = np.loadtxt(
        r'data/fitdata.txt', unpack=True)
sm_ellipse, sq_ellipse = np.loadtxt(
    r'data/ellipse.txt', unpack=False, delimiter=',')
ut.results(nstep,
           (m_linear, sm_linear, q_linear, sq_linear, cov_linear),
           (m_numerical, sm_numerical, q_numerical, sq_numerical),
           (t_MMQ, t_MMQL, gdl_MMQ, t_alpha, tm_corr, tq_corr))

# Create plots/ folder
current_directory = os.getcwd()
plot_directory = os.path.join(current_directory, 'plots')
if not os.path.exists(plot_directory):
    os.makedirs(plot_directory)

# Plot data
plot_MMQL(data, nstep, m_linear, q_linear)
plot_ellipse(nstep, m_numerical, q_numerical, sm_ellipse, sq_ellipse)
plot_MMQN(data, nstep, m_numerical, q_numerical)
plot_TDI(nstep, gdl_MMQ, t_alpha, t_MMQ, t_MMQL)
