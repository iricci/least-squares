#!/usr/bin/env python
# Production

import os
import gc
import numpy as np
import scipy.stats as sst
import progressbar as pb

import src.utilities as ut

# Funzioni______________________________________________________________________


def s(i, j, data):
    """
    S:                calculates partial sums for MMQL routine

    Parameters: i:    integer - power for x_i in partial sum
                j:    integer - power for y_i in partial sum
                data: list    - data from linear relation to analyze 
                                (x, y, sy)
    Output:     sij:  float   - partial sum 

    """
    sij = sum([(data[0][l]**i)*(data[1][l]**j)/(data[2][l]**2.)
              for l in range(len(data[0]))])
    return sij


def chi2(data, m, q,
         summed=True):
    '''
    CHI2:             calculates chi squared function for MMQ and TDI

    Parameters: m:      float   - m parameter for linear relation
                q:      float   - q parameter for linear relation
                data:   list    - data from linear relation to analyze 
                                  (x, y, sy)
                summed: boolean - switches between 'data' as a list of lists, or list of floats
    Output:     chi2:   float   - chi squared function value in (m,q) using data
    '''
    if summed == True:
        chi2 = float(np.sum([((data[1][i] - m*data[0][i] - q)**2.) /
                     (data[2][i]**2.) for i in range(len(data[0]))]))
    else:
        # NOTE: in this instance, data[i] is a float and not a list
        chi2 = ((data[1] - m*data[0] - q)**2.)/(data[2]**2.)
    return chi2


# 1: Minimi Quadrati Lineari___________________________________________________________
def MMQL(data, n):
    """
    MMQL:       derives the Linear Least Squares parameters m, q, from the initial 
                data, with the proper variances

    Parameters: data:   list - starting data for linear relation y = mx+q (x, y, sy)
    Output:     param : list - LLS parameters for the fit line, including errors
                               (m, sm, q, sq, cov)
    """
    s00 = s(0, 0, data)
    s10 = s(1, 0, data)
    s01 = s(0, 1, data)
    s11 = s(1, 1, data)
    s20 = s(2, 0, data)

    d = s00*s20 - s10**2.
    m = (s11*s00 - s01*s10)/d
    sm = s00/d
    q = (s20*s01 - s11*s10)/d
    sq = s20/d
    cov = -s10/d

    # PLOTS
    return float(m), float(np.sqrt(sm)), float(q), float(np.sqrt(sq)), float(cov)


def MMQ(data, numerical_param, data_name):
    """
    MMQ:        derives the Numerical Least Squares parameters m, q, from the initial 
                data, with the proper variances

    Parameters: data:            list - starting data for linear relation y = mx+q (x, y, sy)
                numerical_param: list - numerical parameters needed for the numerical fit; 
                                        includes:
                                            n:        float - m,q grid, row size
                                            extremes: list  - m,q extremal values
                                            eps:      float - delta value to confront minimum
                                                              XX and XX_min+1
    Output:     param:           list - LLS parameters for the fit line, including errors
                                        (m, sm, q, sq)
    """
    # 0. Initialization
    gc.collect()
    XX, m2, q2, m_err, q_err, sm, sq, XX_tensor = ([] for i in range(8))
    n = numerical_param[0]
    m_min, m_max, q_min, q_max = numerical_param[1]
    eps = numerical_param[2]

    # 1. Generating m,q grid using set extremal values
    m = np.linspace(m_min, m_max, n)
    q = np.linspace(q_min, q_max, n)

    # 2. Calculating XX values for each value in the m,q grid
    print("MMQ - Calculating XX...")
    mm, qq = np.meshgrid(m, q)
    for i in pb.progressbar(range(len(data[0])), redirect_stdout=True):
        XXi = chi2((data[0][i], data[1][i], data[2][i]), mm, qq, summed=False)
        XX_tensor.append(XXi)

    XX_matrix = np.sum(XX_tensor, axis=0)
    XX = XX_matrix.flatten()
    m2 = mm.flatten()
    q2 = qq.flatten()

    XX_min = min(XX)
    m2_min = m2[list(XX).index(XX_min)]
    q2_min = q2[list(XX).index(XX_min)]

    # 3. Calculating errors for estimated parameters using numerical methods
    print("MMQ - Estimating errors...")
    XX_err = [XX_min+1.] * len(XX)
    XX_eq = np.isclose(XX, XX_err, rtol=eps)

    for i in pb.progressbar(range(len(XX)), redirect_stout=True):
        if XX_eq[i] == True:
            m_err.append(m2[i])
            q_err.append(q2[i])

    for i in range(len(m_err)):
        sm.append(np.abs(m_err[i] - m2_min))
        sq.append(np.abs(q_err[i] - q2_min))

    # 4. Plot
    return m2_min, max(sm), m_err, q2_min, max(sq), q_err, len(m_err)


def TDI(data, est_m, est_q, n):

    # 1. Chi2
    t_chi_MMQL = chi2(data, est_m[0], est_q[0])
    t_chi_MMQ = chi2(data, est_m[2], est_q[2])
    gdl_MMQ = len(data[0]) - (len(est_m)+len(est_q)) / \
        4.  # being est_param comprised by m,sm couples

    t_alpha_005 = sst.chi2.ppf(1-0.05, gdl_MMQ)

    # 1.1 Plot
    # 2. Compatibilità
    t_comp_m = (est_m[0] - est_m[2])**2./(est_m[1] - est_m[3])**2.
    t_comp_q = (est_q[0] - est_q[2])**2./(est_q[1] - est_q[3])**2.

    return t_chi_MMQ, t_chi_MMQL, gdl_MMQ, t_alpha_005, t_comp_m, t_comp_q


# -MAIN-------------------------------------------------
gc.collect()
# Initialization
data_name = 'Molle1.5'
input_directory = os.path.join(os.path.dirname(__file__), 'input',
                               'dati_molle_1.5_edited.xlsx')
names = ['G', 'M', 'S']
# numerical_param = (10000, (1.1,2.15,-0.0009,0.0009), 1e-5)   #MMQ parameters for numerical estimation
# MMQ parameters for numerical estimation
numerical_param = (1000, (1.1, 2.15, -0.0009, 0.0009), 1e-3)

# Get data from .xlsx file
data = ut.excel(input_directory, names)

# Run parameter estimation
m_linear, sm_linear, q_linear, sq_linear, cov_linear = MMQL(
    data, numerical_param[0])
m_numerical, sm_numerical, sm_ellipse, q_numerical, sq_numerical, sq_ellipse, num_points = MMQ(
    data, numerical_param, data_name)
est_m = (m_linear, sm_linear, m_numerical, sm_numerical)
est_q = (q_linear, sq_linear, q_numerical, sq_numerical)
t_MMQ, t_MMQL, gdl_MMQ, t_alpha, tm_corr, tq_corr = TDI(
    data, est_m, est_q, numerical_param[0])

# Create the data/ folder
current_directory = os.getcwd()
output_directory = os.path.join(current_directory, 'data')
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

# Save data to file
data_to_save = (numerical_param[0],
                m_linear, sm_linear, q_linear, sq_linear, cov_linear,
                m_numerical, sm_numerical, q_numerical, sq_numerical,
                t_MMQ, t_MMQL, gdl_MMQ, t_alpha, tm_corr, tq_corr)
header = ('nstep,'
          'm_linear, sm_linear, q_linear, sq_linear, cov_linear,'
          'm_numerical, sm_numerical, q_numerical, sq_numerical,'
          't_MMQ, t_MMQL, gdl_MMQ, t_alpha, tm_corr, tq_corr')
np.savetxt('data/fitdata.txt', data_to_save,
           header='rows:'+header, delimiter=',')

ellipse = (sm_ellipse, sq_ellipse)
np.savetxt('data/ellipse.txt', ellipse,
           header='rows:m,q', delimiter=',')
