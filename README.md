# least-squares: a simple implementation of linear and numerical Least Squares fitting 

## Scope
This project is modeled after the data analysis part of final exam for the Laboratory II course, held during the second year of the bachelor's degree in Physics at the University of Trieste.

The project stems from a single-file, local Python script hastily developed for the final exam. It has been further adapted to fit the reproducible research [template](https://framagit.org/coslo/template-project) by Prof. Daniele Coslovich. Due to the non-critical "toy" nature of this work, some not-so-optimal coding practices are to be expected.

This project is based on Python and bash scripts, to be executed by terminal. 

## Installation

From the code repository

```
git clone https://framagit.org/iricci/least-squares.git
cd least-squares
```

## Description

The project can be run in its completeness with the command `./make all` (recommended) or it can be run step by step as explained below.

### Step 1: Creating virtual setup

With the command `./make setup` a virtual environment is created and all the needed dependencies are installed. 

Dependencies:
- `numpy` (1.24.3)
- `pandas` (2.0.1)
- `openpyxl` (3.1.2)
- `scipy` (1.10.1)
- `matplotlib` (3.7.1)
- `progressbar` (2.5)

### Step 2: Analysis

The command `./make analysis` reproduces the analysis done in the reaserch. The inputs needed for the reference run are included in the folder inputs/. 
Other inputs can also be included as text files in this folder.

### Step 3: Plots

Plots can be reproduced and numerical results can be shown by running `./make plots`.

### Step 4: Cleanup

The command `./make clean` removes the folders `data/` and `plots/` altogether, clearing all analysis artifacts.

In addition to this, `./make veryclean` also removes the whole
Python environment in `env/` folder.

## To do:
- [ ] Add reference data from an earlier run
- [ ] Refactor `01_production.py` and `02_analysis.py`

## Contributing:

Due to the toy nature of this project, contributions shouldn't be necessary -
yet they are warmly accepted! Reach out to me via my [personal
page](https://iricci.frama.io).

## Authors:
- Iacopo Ricci

Thanks to Prof. Daniele Coslovich for providing the reproducible research template.
