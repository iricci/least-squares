#!/bin/bash


# Check requirements

# Require python
if [[ ! -x $(command -v python3) ]] ; then
    echo python 3 is not installed.
    echo The setup will stop.
    exit 1
fi



# The necessary python dependencies are installed in a python virtual environment (=env/=). The dependencies are also listed in =requirements.txt=.

python3 -m venv env
if [ $? != 0 ] ; then
    echo "Python venv module is not installed, the setup will stop."
    echo "You can install venv from your software package manager."
    echo "Alternatively, install the required packages in your python environment"
    echo "with the command: pip install -r requirements.txt"
    exit 1
fi

. env/bin/activate
pip install -r requirements.txt
deactivate
