"""
MOLLE:      Analisi dati sulla costante elastica di una molla elicoidale
            UTILITIES
Autore:     Iacopo Ricci
"""
import os
import numpy as np
import pandas as pd


def excel(inp, names):
    """
    EXCEL:      gets sample data from specified path, using selected columns 

    Parameters: path:  string - path containing sample data; .xlsx file
                names: list   - contains names for the columns to gather
    Output:     data:  list   - selected data from linear relation to analyze 
                                (x, y, sy)

    """

    assert os.path.exists(inp), f"Input path {inp} doesn't exist"
    df = pd.read_excel('%s' % inp)

    x = np.asarray(pd.DataFrame(df, columns=list(names[0])))
    y = np.asarray(pd.DataFrame(df, columns=list(names[1])))
    sy = np.asarray(pd.DataFrame(df, columns=list(names[2])))
    data = (x, y, sy)

    return data


def results(n, param_MMQL, param_MMQ, param_TDI):
    # 1. Results on stdout
    print('-------------------------------------------------------------------')
    print('1. Parameter estimation')
    print('1.1 MMQL')
    print('m = {}, sm = {}' .format(param_MMQL[0], param_MMQL[1]))
    print('q = {}, sq = {}' .format(param_MMQL[2], param_MMQL[3]))
    print('cov_mq = {}' .format(
        float(param_MMQL[4]/(param_MMQL[1]*param_MMQL[3]))))
    print('1.2. MMQ')
    print('m = {}, sm = {}' .format(param_MMQ[0], param_MMQ[1]))
    print('q = {}, sq = {}' .format(param_MMQ[2], param_MMQ[3]))
    print('2. Hypotesis testing')
    print('2.1 Chi2')
    print('t_MMQL : {}, t_MMQ : {}, GDL: {}' .format(
        param_TDI[0], param_TDI[1], param_TDI[2]))
    print('Significance 0.05: {}' .format(param_TDI[3]))
    print('2.2 Compatibility')
    print('t_m : {}, t_q : {}' .format(param_TDI[4], param_TDI[5]))
    print('-------------------------------------------------------------------')


"""
def timelog(start_time, data_name, n, eps, num_points,
            path="data/exectimes.txt"):
    '''
    DESCRIPTION: execution times are logged to 'exectimes.txt' file;  
    VARIABLES:
        INPUT: start_time       clock time of program start
        OUTPUT:None
    EXTERNAL CALLS: None
    '''
    import time
    import datetime

    assert os.path.exists(out), f"Input path {inp} doesn't exist"
    filename = os.path.join(os.path.dirname(__file__),path)
    if os.path.exists(filename) == False:
        with open(filename,'w') as f:
            f.write('DateTime, DataName, GridSize, #Points, EpsSize, #EllipsePoints, Exec. time \n')
    
    with open(filename,'a') as f:
        seconds = time.time() - start_time
        print("Elapsed time: %s" % (datetime.timedelta(seconds = seconds)))
        f.write('{},{},{},{},{},{},{} \n' .format(str(datetime.datetime.now()),data_name,n,n*n,eps,num_points,datetime.timedelta(seconds = seconds)))
        f.close()
"""
