#!/bin/bash
# Helpers                                                          :noexport:

# Bash logger

start() {
    start=$(date +%s.%N)
}
stop() {
    [[ ! -x $(command -v bc) ]] && return
    [[ ! -z $@ ]] && msg="$@" || msg="done"
    if [[ ! -z $start ]] ; then
	duration=$(echo $(date +%s.%N) - $start | bc)
	interval=$(LC_NUMERIC="en_US.UTF-8" printf "%.1f" $duration)
	echo "...$msg in $interval seconds"
    fi
}
